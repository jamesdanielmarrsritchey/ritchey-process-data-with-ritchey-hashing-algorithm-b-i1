<?php
#Name:Process Data With Ritchey Hashing Algorithm B v3
#Description:Process data with Ritchey Hashing Algorithm B v3. Returns Ritchey Hashing Algorithm B v3 checksum as a string success. Returns "FALSE" on failure.
#Notes:Optional arguments can be "NULL" to skip them in which case they will use default values.
#Arguments:'string' is a string containing the data to hash. 'max_length' (optional) is a number specifying the maximum length to allow the checksum to be. This value is used to determine when increment reset occurs. Higher values are stronger. It's best to use even numbers to ensure max_length can be fully utilized. 'include_metadata' (optional) is a boolean indicating whether to include metadata along with the checksum in the return. 'display_errors' (optional) indicates if errors should be displayed.
#Arguments (Script Friendly):string:string:required,max_length:number:optional,include_metadata:bool:optional,display_errors:bool:optional
#Content:
if (function_exists('process_data_with_ritchey_hashing_algorithm_b_v3') === FALSE){
function process_data_with_ritchey_hashing_algorithm_b_v3($string, $max_length = NULL, $include_metadata = NULL, $display_errors = NULL){
	$errors = array();
	$progress = '';
	if (@isset($string) === FALSE){
		$errors[] = "string";
	}
	if ($max_length === NULL){
		$max_length = FALSE;
	} else if (@is_int($max_length) === TRUE){
		#Do Nothing
	} else if ($max_length === FALSE){
		#Do Nothing
	} else {
		$errors[] = "max_length";
	}
	if ($include_metadata === NULL){
		$include_metadata = FALSE;
	} else if ($include_metadata === TRUE){
		#Do Nothing
	} else if ($include_metadata === FALSE){
		#Do Nothing
	} else {
		$errors[] = "include_metadata";
	}
	if ($display_errors === NULL){
		$display_errors = FALSE;
	} else if ($display_errors === TRUE){
		#Do Nothing
	} else if ($display_errors === FALSE){
		#Do Nothing
	} else {
		$errors[] = "display_errors";
	}
	##Task []
	if (@empty($errors) === TRUE){
		$checksum = 'a';
		###Convert each byte individually to a number, and increment $checksum by that. If $checksum exceeds max_length reset to a, and add the number of resets done to the next number.
		$string = @str_split($string, 1);
		$resets = 0;
		$previous_value = 0;
		foreach ($string as &$value){
			####Convert byte to decimal representation, and add "1" in case the number returned is "0".
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/data_to_decimal_representation_v1/data_to_decimal_representation_v1.php';
			$value = @implode(data_to_decimal_representation_v1($value, FALSE));
			$value = $value + 1;
			$value = $value + $previous_value;
			if ($value > 256){
				$value = $value - 256;
			}
			$value = $value + $resets;
			####Increment checksum by number, but if checksum exceeds max_length reset to "a"
			$location = realpath(dirname(__FILE__));
			require_once $location . '/dependencies/increment_text_b_v1/increment_text_b_v1.php';
			for ($i = 1; $i <= $value; $i++) {
				$checksum = increment_text_b_v1($checksum, FALSE);
				if ($max_length === FALSE){
					#Continue without wrapping
				} else {
					if (@strlen($checksum) > $max_length){
						$checksum = 'a';
						$resets++;
					}
				}
			}
			$previous_value = $value;
		}
		###If include_metadata is TRUE, add to return
		if ($include_metadata === TRUE){
			if ($max_length === FALSE){
				$max_length = 'FALSE';
			}
			$checksum = "Options:max_length:{$max_length},Value:{$checksum}";
		}
	}
	result:
	##Display Errors
	if ($display_errors === TRUE){
		if (@empty($errors) === FALSE){
			$message = @implode(", ", $errors);
			if (function_exists('process_data_with_ritchey_hashing_algorithm_b_v3_format_error') === FALSE){
				function process_data_with_ritchey_hashing_algorithm_b_v3_format_error($errno, $errstr){
					echo $errstr;
				}
			}
			set_error_handler("process_data_with_ritchey_hashing_algorithm_b_v3_format_error");
			trigger_error($message, E_USER_ERROR);
		}
	}
	##Return
	if (@empty($errors) === TRUE){
		return $checksum;
	} else {
		return FALSE;
	}
}
}
?>